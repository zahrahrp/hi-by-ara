<!DOCTYPE html>
<html lang="en">
  <head>
    <title>HIBYARA </title>
    <link rel="stylesheet" href="/css/shared.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lora&display=swap" rel="stylesheet">

    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
      integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
      crossorigin="anonymous"
    ></script>

    <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous"
    />

    <script
      src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
      integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
      crossorigin="anonymous"
    ></script>

    <link
      href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp"
      rel="stylesheet"
    />

    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.12.0/css/all.css"
      integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M"
      crossorigin="anonymous"
    />
    <link
      href="https://fonts.googleapis.com/icon?family=Poppins"
      rel="stylesheet"
    />

     <style>
        body {
            background-color:#fef29e;
            font-family: 'Lora', serif;
            color:#32302b;
        }

        .center {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            padding: 10px;
        }

        h2 {
            padding:10px;
        }

        .footer {
            position: absolute;
            left: 50%;
            bottom: 0;
            transform: translate(-50%, -50%);
            padding:10px;
        }

        .card{
            margin: 20px;
            border-color: white;
            border-radius: 20px;
        }

        .link-a{
            margin: 20px;
        }

    </style>
  </head>

  <body>

    @yield('container')

    <div class='footer'>
        <a href="https://gitlab.com/zahrahrp/hi-by-ara" style="color:#32302b;"><u>Made by Ara </u></a>
    </div>
  </body>

</html>
