@extends('layouts.template')

@section('container')
    <div style="padding:20px;">
        <a href="{{ url('hellos') }}" style="color:#32302b;">back </a>
    </div>

    <div class="content center" style="text-align:center;">
                  <br>
                  <?php
                   echo Form::open(array('url' => 'store'));

                      echo '<h4> How do you say hello in your language? </h4>';
                      echo Form::text('word','');
                      echo '<br><br>';

                      echo '<h4> What is the language? </h4>';
                      echo Form::text('language', '');
                      echo '<br>';

                      echo '<br>';
                      echo Form::submit('Add!');
                   echo Form::close();
                ?>
                <br>
    </div>
@endsection

