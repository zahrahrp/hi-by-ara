@extends('layouts.template')

@section('container')
    <div class="container" style="padding:20px;margin-left:10px">
        <div class="row">
            <div class="col-sm-4">
                <a href="{{ url('') }}" style="color:#32302b;text-align:left;">back to homepage </a>
            </div>
            <div class="col-sm-8">
                <a href="{{ url('about') }}" style="color:#32302b;margin-left:180px;" class="link-a">about </a>
                <a href="{{ url('hellos') }}" style="color:#32302b;margin-left:50px;" class="link-a">list words </a>
            </div>
        </div>
    </div>

    <div class="content" style="text-align:center;">
        <h2> What is Hibyara? </h2>
        <br>
        <p> “Hello!”, “Hi!”, and “Hey!” are the three most common forms of greetings in use today. <br>
            Greetings do not rely only on the language, but also on the way you express it. <br>
            Different countries have their different customs of saying “hello” to each other. <br>
            Knowing how to say hello in different languages of the world and <br>
            which conversation opener to use is the first step in learning a new language
        </p>
        <p> This website is a place where you can find out how do people around the world say hello.  <br>
            P.S. you can add your own native language :)
        </p>
        <a href="{{ url('hellos') }}" style="color:#32302b;margin-left:50px;" class="link-a"><u>explore list of 'hellos'!<u></a>
    </div>
@endsection
