{{-- @dd($list_hellos) --}}

@extends('layouts.template')

@section('container')
<div class="container" style="padding:20px;margin-left:10px">
    <div class="row">
        <div class="col-sm-4">
            <a href="{{ url('') }}" style="color:#32302b;text-align:left;">back to homepage </a>
        </div>
        <div class="col-sm-8">
            <a href="{{ url('about') }}" style="color:#32302b;margin-left:180px;" class="link-a">about </a>
            <a href="{{ url('hellos') }}" style="color:#32302b;margin-left:50px;" class="link-a">list words </a>
        </div>
    </div>
</div>

<div class="content" style="text-align:center;">
      <h2> How Do People Say Hello Around the World? </h2>
      <a href="{{ url('form') }}" style="color:#32302b;"> add how you say hello in your language! </a><br>
</div>

<div class="row">

    @foreach ( $list_hellos as $hello)
        <div class="col-sm-3 col-md-2" style="padding:20px;color:#32302b;">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title" style="text-align:center;"> {{ $hello->word }}</h4>
                <p class="card-text"> - {{ $hello->language }}</p>
              </div>
            </div>
          </div>
    @endforeach

</div>
@endsection

