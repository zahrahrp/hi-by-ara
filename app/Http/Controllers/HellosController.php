<?php

namespace App\Http\Controllers;

use App\Hellos;
use Illuminate\Http\Request;

class HellosController extends Controller
{
    //
    public function create() {
        return view('forms');
    }
    public function showAll() {
        return view('hellos', [
            "list_hellos" => Hellos::all(),
        ]);
    }

    public function store() {
        $hellos = new Hellos();
        $hellos->word = request('word');
        $hellos->language = request('language');
        $hellos->save();

        return redirect('/hellos');
    }
}
