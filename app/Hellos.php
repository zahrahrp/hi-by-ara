<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hellos extends Model
{
    //
    protected $table = 'Hellos';

    protected $fillable = ['word', 'language'];


}
